


UPDATE aplicacion.factura_ventas  
   SET 
        gravada0= t.gravada0, 
        gravada5= t.gravada5,    
        gravada10= t.gravada10, 
        monto_total = t.monto_total, 

        iva5 = (t.gravada5 / 21), 
        iva10 = (t.gravada10 / 11), 
        total_iva = (t.gravada5 / 21) + (t.gravada10 / 11) , 
 
        importe_vuelto = importe_pagado - t.monto_total 
  
from  
( 
SELECT factura, sum(sub_total) monto_total , sum(gravada0) gravada0 , sum(gravada5) gravada5 , sum(gravada10) gravada10 
  FROM aplicacion.factura_ventas_detalle 
where factura = v0 
group by factura  
) as t  
 WHERE factura_ventas.factura = v0  
 and t.factura = factura_ventas.factura  



