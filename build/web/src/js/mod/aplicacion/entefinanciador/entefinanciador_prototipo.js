

function EnteFinanciador(){
    
   this.tipo = "entefinanciador";   
   this.recurso = "entesfinanciadores";   
   this.value = 0;
   this.form_descrip = "nombre";
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Ente Financiador"
   this.tituloplu = "Entes Financiadores"   
      
   
   this.campoid=  'ente_financiador';
   this.tablacampos =  ['ente_financiador', 'nombre'];
   
   this.etiquetas =  ['Ente', 'Nombre'];                                  
    
   this.tablaformat = ['C', 'C'];                                  
   
   this.tbody_id = "entefinanciador-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "entefinanciador-acciones";   
         
   this.parent = null;
   
   
}





EnteFinanciador.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
};





EnteFinanciador.prototype.form_ini = function() {    
    
};






EnteFinanciador.prototype.form_validar = function() {    
    
   
    var nombre = document.getElementById('entefinanciador_nombre');    
    if (nombre.value == "")         
    {
        msg.error.mostrar("Campo de nombre vacio");           
        nombre.focus();
        nombre.select();        
        return false;
    }              
    
    return true;
};










EnteFinanciador.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            
           

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};






EnteFinanciador.prototype.combobox = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.rel()+'/api/'+this.recurso+'/all' ;    
    //var data = {username: 'example'};
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));

    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {

            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            
            

            var oJson = JSON.parse( data ) ;

            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['ente_financiador'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        .catch(function(error) {
            console.log(error);            
        });


}


