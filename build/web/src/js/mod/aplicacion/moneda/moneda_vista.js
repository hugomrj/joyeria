
var moneda_vista = 
{      
                        
    lista_paginacion: function( obj, page ) 
    {

        const promise = new Promise((resolve, reject) => {

            loader.inicio();

            var comp = window.location.pathname;                 
            var path =  comp.replace(arasa.html.url.rel(), "");

            var xhr =  new XMLHttpRequest();            
            var url = arasa.html.url.rel()+"/api/monedas"; 
            
            url = url + "?page=" + page;
            
            //var url = arasa.html.url.rel()+"/api/monedas?page=2"; 
            var metodo = "GET";                         
            ajax.json  = null;

            xhr.open( metodo.toUpperCase(),   url,  true );      


            xhr.onreadystatechange = function () {
                if (this.readyState == 4 ){

                    moneda_vista.tabla_html(obj);
                    
                    ajax.local.token =  xhr.getResponseHeader("token") ;            
                    localStorage.setItem('token', xhr.getResponseHeader("token") );     
                    //sessionStorage.setItem('total_registros',  ajax.xhr.getResponseHeader("total_registros"));
                    
                    
                    // crear y cargar la tabla html
                   
                    
                    var ojson = JSON.parse( xhr.responseText ) ;                        
                    
                    tabla.json = JSON.stringify(ojson['datos']) ;
                                                           

                    tabla.ini(obj);
                    tabla.gene();   
                    tabla.formato(obj);
                    
                    // gemerar la paginacion                     
                    
                   // los botones tal vez tenga que ir en el main
                  
                  
                   var json_paginacion = JSON.stringify(ojson['paginacion']);
                   
                   moneda_vista.paginacion_html(obj, json_paginacion );





                    ajax.state = xhr.status;

                        resolve( xhr );

                    loader.fin();


                }
            };
            xhr.onerror = function (e) {                    
                reject(
                      xhr.status,
                      xhr.response   
                );                 

            };                       

            xhr.setRequestHeader("path", path );
            var type = "application/json";
            xhr.setRequestHeader('Content-Type', type);   
            
            //ajax.headers.set();
            xhr.setRequestHeader("token", localStorage.getItem('token'));           

            xhr.send( ajax.json  );                       



        })

        return promise;

    },



    tabla_html: function( obj  ) {   
                
        if (!(obj.alias === undefined)) {    
            ajax.url = html.url.rel() + obj.carpeta +'/'+ obj.alias + '/htmf/lista.html';    
        }              
        else{
            ajax.url = obj.carpeta +'/'+ obj.tipo + '/htmf/lista.html';    
        }
        
        ajax.metodo = "GET";            
        document.getElementById( obj.dom ).innerHTML =  ajax.public.html(   );
        reflex.getTituloplu(obj);
        
        // tal vez esto se tenga que cambiar con una promesa
        //ajax.get.html( url );

    },



    paginacion_html: function( obj , json ) {   
                
                /*

        document.getElementById( obj.tipo + "_paginacion" ).innerHTML 
                = paginacion.gene();     

                */
               

        arasa.html.paginacion.ini(json);
        
        document.getElementById( obj.tipo + "_paginacion" ).innerHTML 
                = arasa.html.paginacion.gene();   
        
        
        //arasa.html.paginacion.move(obj, buscar, reflex.form_id);
        arasa.html.paginacion.move(obj, "", reflex.form_id);
        
         //paginacion.move( obj, buscar, reflex.form_id );        
        
        
        
        

    },






}










  /**
   * Native XMLHttpRequest as Promise
   * @see https://stackoverflow.com/a/30008115/933951
   * @param {Object} options - Options object
   * @param {String} options.method - Type of XHR
   * @param {String} options.url - URL for XHR
   * @param {String|Object} options.params - Query parameters for XHR
   * @param {Object} options.headers - Query parameters for XHR
   * @return {Promise} Promise object of XHR
   */
  
  
//  function _xhrPromise (options){
function _xhrPromise (){      
      
    return new Promise((resolve, reject) => {
        
      let xhr    = new XMLHttpRequest();
      let params = options.params;

      //xhr.open(options.method, options.url);


        xhr.open("GET", "https://jsonplaceholder.typicode.com/posts/");


      xhr.onload = () => {
        if (xhr.status >= 200 && xhr.status < 300) {
            //resolve(JSON.parse(xhr.response));
            
            resolve(
                    xhr.status,
                    xhr.response
                    );
            
        } 
        else {
          reject({
            status: xhr.status,
            statusText: xhr.statusText
          });
        }
      };

      xhr.onerror = () => {
        reject({
          status: xhr.status,
          statusText: xhr.statusText
        });
      };


      // Set headers
      if (options.headers) {
        Object.keys(options.headers).forEach((key) => {
          xhr.setRequestHeader(key, options.headers[key]);
        });
      }

      // We'll need to stringify if we've been given an object
      // If we have a string, this is skipped.
      if (params && typeof params === `object`) {
        params = Object.keys(params).map((key) => {
          return `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`;
        }).join(`&`);
      }

      //xhr.send(params);
      xhr.send();
    });
  }
  
  
  
  
  
  
  
  
  
  function get(url) { 
  // Return a new promise.
  return new Promise(function(resolve, reject) {
    // Do the usual XHR stuff
    var req = new XMLHttpRequest();
    req.open('GET', url);

    req.onload = function() {
      // This is called even on 404 etc
      // so check the status
      if (req.status == 200) {
        // Resolve the promise with the response text
        resolve(req.response);
      }
      else {
        // Otherwise reject with the status text
        // which will hopefully be a meaningful error
        reject(Error(req.statusText));
      }
    };

    // Handle network errors
    req.onerror = function() {
      reject(Error("Network Error"));
    };

    // Make the request
    req.send();
  });
}

