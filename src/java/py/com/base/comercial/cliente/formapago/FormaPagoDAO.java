/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.cliente.formapago;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;


public class FormaPagoDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public FormaPagoDAO ( ) throws IOException  {
    }
      
      

    public List<FormaPago>  all () {
                
        List<FormaPago>  lista = null;        
        try {                        
                        
            FormaPagoRS rs = new FormaPagoRS();            
            lista = new Coleccion<FormaPago>().resultsetToList(
                    new FormaPago(),
                    rs.all()
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      


}
