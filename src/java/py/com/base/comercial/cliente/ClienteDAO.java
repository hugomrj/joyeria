/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.comercial.cliente;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;


/**
 *
 * @author hugom_000
 */

public class ClienteDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public ClienteDAO ( ) throws IOException  {
    }
      
    
    public List<ClienteExt>  list (Integer page) {
                
        List<ClienteExt>  lista = null;        
        try {                        
                        
            ClienteRS rs = new ClienteRS();            
            
            lista = new Coleccion<ClienteExt>().resultsetToList(
                    new ClienteExt(),
                    rs.list(page)                    
            );                      
            
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    
    
    
    
    public List<ClienteExt>  search (Integer page, String busqueda) {
                
        List<ClienteExt>  lista = null;
        
        try {                       
                        
            ClienteRS rs = new ClienteRS();
            lista = new Coleccion<ClienteExt>().resultsetToList(
                    new ClienteExt(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
          
        
    
    public Cliente identificador(String identi) throws Exception {      

          Cliente cliente = new Cliente();  

          if (identi.contains("-")){
              identi = "R"+identi;
          }
          else{
            identi = "C"+identi+"-0";   
          }
          
          String sql = new ClienteSQL().identificador(identi);


          cliente = (Cliente) persistencia.sqlToObject(sql, cliente);

          return cliente;          

      }
         
        
    
    public Cliente getClienteDocNumero (String tipo, String numero, Integer digito ) 
            throws Exception {      

          Cliente cliente = new Cliente();  
          
          String sql = new ClienteSQL().getClienteDocNumero( tipo, numero, digito);
                
          cliente = (Cliente) persistencia.sqlToObject(sql, cliente);

          return cliente;          

      }
    
    
    
    
}
