/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.aperturadiaria;

import java.util.Date;


/**
 *
 * @author hugom_000
 */
public class AperturaDiaria {
    
    private Integer apertura;    
    private Date fecha;

    public Integer getApertura() {
        return apertura;
    }

    public void setApertura(Integer apertura) {
        this.apertura = apertura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    
    
    
    
}

