/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.identificador_documento;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;


public class IdentificadorDocumentoDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public IdentificadorDocumentoDAO ( ) throws IOException  {
    }
      
      

    public List<IdentificadorDocumento>  all () {
                
        List<IdentificadorDocumento>  lista = null;        
        try {                        
                        
            IdentificadorDocumentoRS rs = new IdentificadorDocumentoRS();            
            lista = new Coleccion<IdentificadorDocumento>().resultsetToList(
                    new IdentificadorDocumento(),
                    rs.all()
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      


}
