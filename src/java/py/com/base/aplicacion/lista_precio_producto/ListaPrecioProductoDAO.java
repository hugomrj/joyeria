/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.base.aplicacion.lista_precio_producto;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;


/*
 * @author hugom_000
 */

public class ListaPrecioProductoDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public ListaPrecioProductoDAO ( ) throws IOException  {    
        
    }
      
    
    


    public List<ListaPrecioProducto>  cabProducto (Integer producto, Integer page) {
                
        List<ListaPrecioProducto>  lista = null;        
        try {          
            
            ListaPrecioProductoRS rs = new ListaPrecioProductoRS();      
            
            lista = new Coleccion<ListaPrecioProducto>().resultsetToList(
                    new ListaPrecioProducto(),
                    rs.cabProducto(producto, page)
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

      
    
    
    

        
}
