/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.sistema.usuario;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;


import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.xml.Global;
import nebuleuse.seguridad.Autentificacion;

/**
 * REST Web Service
 * @author hugo
 */


@Path("usuarios")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class UsuarioWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
    private Response.Status status  = Response.Status.OK;
    
    
    Usuario com = new Usuario();       
                         
    public UsuarioWS() {
    }

    
    
    
    @GET    
    public Response list ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page) {
        
            if (page == null) {                
                page = 1;
            }

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                                
                JsonObject jsonObject = new UsuarioJSON().lista(page);
            
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString())
                        .header("token", autorizacion.encriptar())                        
                        .build();                       
            }
            else{
                // se puede generar un response de tiempo excedido
                throw new Exception();
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }      
    }    
    
      
    
    
    
    @GET       
    //@Path("/search/{opt : (.*)}") 
    @Path("/search/") 
    public Response search ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page,              
            @MatrixParam("q") String q
            ) {
        
        
            if (page == null) {                
                page = 1;
            }
            if (q == null){            
                q = "";                
            }

            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                UsuarioDAO dao = new UsuarioDAO();
                
                List<Usuario> lista = dao.search(page, q);
                String json = gson.toJson( lista );     
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())                        
                        .build();                       
            }
            else{
                // se puede generar un response de tiempo excedido
                throw new Exception();
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }      
    }    
    
    
    
    
    @GET
    @Path("/{usu}")
    public Response getUsuario(     
            @HeaderParam("token") String strToken,
            @PathParam ("usu") Integer usu ) {
                     
        try 
        {                  
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();                
                this.com = (Usuario) persistencia.filtrarId(com, usu);  
        
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;                           
                }else
                {
                    this.com.setClave("secret");
                }
                
                String json = gson.toJson(this.com);
                
                return Response
                        .status( this.status )
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                // se puede generar un response de tiempo excedido
                throw new Exception();
            }
        
        }     
        catch (Exception ex) {
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }        

    }    
      
      
    
    
    
 
    @POST
    public Response add( 
            @HeaderParam("token") String strToken,
            String json ) {
                     
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                
                Usuario req = gson.fromJson(json, this.com.getClass());
                
                autorizacion.actualizar();    
                this.com = new UsuarioDAO().insert(req.getCuenta(), req.getClave());
                             
                if (this.com == null){
                    System.out.println("usuario vacio");
                    this.status = Response.Status.NO_CONTENT;
                }
                
                json = gson.toJson(this.com);
                
                return Response
                        .status(this.status)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else
            {                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                                
            }
        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        

    }    
 
      
    
    @PUT    
    @Path("/{usu}")    
    public Response edit (            
            @HeaderParam("token") String strToken,
            @PathParam ("usu") Integer usu,
            String json
            ) {
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
            
                Usuario req = gson.fromJson(json, this.com.getClass());
                this.com = new UsuarioDAO().update(req, usu);                
                
                json = gson.toJson(this.com);
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{    
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                    
    
        }        
    }    
    
    
    
    
    
    
    
    @DELETE  
    @Path("/{id}")    
    public Response deleteUsuario (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id) {
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    
            
                Integer filas = 0;
                //filas = new UsuarioDAO().delete(usu);                
                filas = persistencia.delete(com, id) ;
                
                
                if (filas != 0){
                    return Response
                            .status(Response.Status.OK)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();                       
                }
                else{   
                    
                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();                        
                }
            }
            else
            {  
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();              
            }        
        } 

        catch (Exception ex) {            

            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();           
        }               
        
    }    
        

    
    
    
    
    
    @POST
    @Path("/login")
    public  Response login ( String json, @Context HttpServletRequest request ) throws IOException 
      {
       
            
            String remotehost = request.getRemoteHost();            
            
            String real_ip = request.getHeader("X-Forwarded-For");            
            
            Date fecha = new Date();
            String sistema = new Global().getValue("sistema");
            
            //  {"cuenta":"usu","clave":"pass", "sucursal" :{  "sucursal": "1"} }
                         
                    
        try {
            
                UsuarioExt usuExt = new UsuarioDAO().setJsonExt(json);
                Usuario usu = new Usuario();
                
             
                
                
                usu  = new UsuarioDAO().login(
                            usuExt.getCuenta(), 
                            usuExt.getClave()
                            );
          
                
                
            if (usu == null)
            {           
                                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .entity("Authorization Required")
                    .header("token", null)
                    .build();                                        
                
            }
            else
            {
                
                
                if (real_ip == null ){                          
                    real_ip = " null " ;
                }
                

                System.out.println( sistema +"  "+ real_ip.toString() +"  " + remotehost + " " + fecha + " " + usu.getCuenta() );            
                
                usu.setClave("secret");   
                           
                
                autorizacion.newTokken(
                        usu.getUsuario().toString(), 
                        usu.getCuenta().trim()
                ) ;

                
                new UsuarioDAO().set_tokenDB(
                        usu.getUsuario().toString(), 
                        autorizacion.token.getIat()
                );
                
                
                return Response
                        .status(Response.Status.OK)
                        .entity(null)
                        .header("token", autorizacion.encriptar())
                        .build();                        
            }                      
        } 
        
        catch (Exception ex) {
            
            
System.out.println("error en login");
System.out.println(ex.getMessage());
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .build();                                        
        }
    }
       


    
    
    
    
    
    
    
    
    @GET    
    @Path("/controlurl") 
    public Response controlurl ( 
            @HeaderParam("token") String strToken,
            @HeaderParam("path") String strPath
            ) {
        
        try {  
                       
            if (autorizacion.verificar(strToken))
            {                  
                
                UsuarioDAO dao = new UsuarioDAO();
                Integer usu = Integer.parseInt( autorizacion.token.getUser() );
               
               
                if ( dao.IsControlPath(usu, strPath ) )
                {
                    return Response
                            .status(Response.Status.OK)           
                            .header("token", strToken)                            
                            .header("sessionusuario", autorizacion.token.getNombre())
                            .build();                     
                }
                else
                {                
                    return Response
                            .status(Response.Status.UNAUTHORIZED)
                            .build();                            
                } 
            }
            else
            {  
                throw new Exception();
            }        
        }     
        catch (Exception ex) {
                return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("0")
                    .build();                                      
        }      
    }    
        
    
    
    
      
     
    @PUT    
    @Path("/cambiopass")    
    public Response cambiopass (            
            @HeaderParam("token") String strToken,
            String json
            ) {
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    

            JsonObject convertedObject = new Gson().fromJson(json, JsonObject.class);            
            
            String old = convertedObject.get("old").getAsString() ;
            String jnew = convertedObject.get("new").getAsString() ;
            String usu = autorizacion.token.getUser();
            
                Integer i = new UsuarioDAO().cambiarPass(usu, old, jnew);
            
                
                if (i == 0)
                {
                    
                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();                                            
                }
                else
                {   
                    return Response
                            .status(Response.Status.OK)
                            .entity(json)
                            .header("token", autorizacion.encriptar())
                            .build();                           

                }            

            }
            else{    
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }     
        catch (Exception ex) {
            
System.out.println(ex.getMessage());
System.out.println(ex.getCause());
System.out.println(ex.getLocalizedMessage());
System.out.println(ex.getClass().toString());


            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    //.entity("Error")
                    .entity(ex.getMessage())
                    .header("token", strToken)
                    .build();                    
    
        }        
    }    
    
    
    
    
        
    

    
    
    
}