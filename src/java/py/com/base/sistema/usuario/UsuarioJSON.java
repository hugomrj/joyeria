/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.sistema.usuario;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.JsonObjeto;
import nebuleuse.ORM.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class UsuarioJSON  {


    
    
    public UsuarioJSON ( ) throws IOException  {
    
    }
      
    
    
    

    public JsonObject  lista ( Integer page) {
        
        
        JsonObject jsonObject = new JsonObject();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

        
        try 
        {   
  

            ResultadoSet resSet = new ResultadoSet();                   
            String sql = SentenciaSQL.select(new Usuario());     
            

            ResultSet rsData = resSet.resultset(sql, page);                            
            UsuarioDAO dao = new UsuarioDAO();                
            List<Usuario> lista = dao.list( rsData );                
                            
            
        
            //datos
            JsonArray jsonArrayDatos = new JsonArray();
            JsonParser jsonParser = new JsonParser();
            jsonArrayDatos = (JsonArray) jsonParser.parse(gson.toJson( lista ));              
            
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);
            
            jsonObject.add("datos", jsonArrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);            
            



        }         
        catch (Exception ex) {                        

            System.out.println(ex.initCause(ex).getMessage());
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
        
    
    
    
        
}
