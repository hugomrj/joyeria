/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nebuleuse.ORM.xml;

import java.util.ArrayList;
import nebuleuse.ORM.Atributo;
import org.w3c.dom.Node;

public class NodoXML {

    private String nombre;    
    private ArrayList<Atributo> atributos = new ArrayList <Atributo>() ;   
    
    //private ArrayList<NodoXML> nodos = new ArrayList <NodoXML>() ;
    
    private String value;    
    private String propiedades;
    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Atributo> getAtributos() {
        return atributos;
    }

    public void setAtributos(ArrayList<Atributo> atributos) {
        this.atributos = atributos;
    }

    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPropiedades() {
        return propiedades;
    }

    public void setPropiedades(String propiedades) {
        this.propiedades = propiedades;
    }
     
    
    public short getNodeType() {
        return Node.ELEMENT_NODE;
    }

    
    
    
    
    
    
    public void addAtributos(String line){
        
        int i = line.indexOf('=');
        
        while ( i > 0)
        {
            Atributo e = new Atributo();
                 

            String nombre = line.substring(0, i).trim();
                    
            e.setNombre(nombre);            
            line = line.replace(nombre, "").trim();            
        
               
            // buscar valor de dupla            
            
            Integer c1 =0, c2 = 0;
            Character co = '"';

            for ( int n = 0; n < line.length (); n++ ) {            
                Character c = line.charAt(n);             
                if ( c.equals( co ) ) {        

                    if (c1 == 0){
                        c1 = n;
                    }
                    else{
                        if (c2 == 0){
                            c2 = n;
                        }
                    }                
                }
            }            
            
        
            e.setValor( line.substring(c1+1, c2).trim() );

            this.atributos.add(e);            
                    
            line = line.replaceFirst("=", "").trim();
            line = line.replaceFirst( '"'+e.getValor()+'"' , "").trim();
            
            i = line.indexOf('=');
            
        }
       
    
    }
    
    
    
    

        
    
    
    
}
