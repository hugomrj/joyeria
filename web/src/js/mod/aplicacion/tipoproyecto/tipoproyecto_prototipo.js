

function TipoProyecto(){
    
   this.tipo = "tipoproyecto";   
   this.recurso = "tiposproyectos";   
   this.value = 0;
   this.form_descrip = "nombre";
   this.json_descrip = "nombre";
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Tipo Proyecto"
   this.tituloplu = "Tipos Proyectos"   
      
   
   this.campoid=  'tipo_proyecto';
   this.tablacampos =  ['tipo_proyecto', 'nombre'];
   
   this.etiquetas =  ['Tipo', 'Nombre'];                                  
    
   this.tablaformat = ['N', 'C'];                                  
   
   this.tbody_id = "tipoproyecto-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "tipoproyecto-acciones";   
         
   this.parent = null;
   
   
}





TipoProyecto.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
};





TipoProyecto.prototype.form_ini = function() {    
    
};






TipoProyecto.prototype.form_validar = function() {    
    
   
    var nombre = document.getElementById('tipoproyecto_nombre');    
    if (nombre.value == "")         
    {
        msg.error.mostrar("Campo de nombre vacio");           
        nombre.focus();
        nombre.select();        
        return false;
    }              
    
    return true;
};










TipoProyecto.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};






TipoProyecto.prototype.combobox = function( dom ) {     
            
    loader.inicio();
                        
    var url = html.url.rel()+'/api/'+this.recurso+'/all' ;    

    //var data = {username: 'example'};
    var headers = new Headers();    
    headers.append('token', localStorage.getItem('token'));




    fetch( url ,
        {
            method: 'GET', 
            headers: headers
        })
        .then(response => {
            return response.text();
        })
        .then(data => {


            var domobj = document.getElementById(dom);
            var idedovalue = domobj.value;            
      
            var oJson = JSON.parse( data ) ;


            for( x=0; x < oJson.length; x++ ) {

                var jsonvalue = (oJson[x]['tipo_proyecto'] );            

                if (idedovalue != jsonvalue )
                {  
                    var opt = document.createElement('option');            
                    opt.value = jsonvalue;
                    opt.innerHTML = oJson[x]['nombre'];                        
                    domobj.appendChild(opt);                     
                }

            }            
            
                //resolve( data );
            loader.fin();
        })
        /*
        .catch(function(error) {
            console.log(error);            
        });
        */

}




