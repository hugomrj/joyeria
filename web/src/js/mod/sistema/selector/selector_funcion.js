


function selector_lista_all ( obj, dom ){
  
        
    var url = arasa.html.url.rel() + obj.carpeta +'/'+ obj.tipo 
            + '/htmf/lista.html'; 
    
    fetch( url )
      .then(response => {
        return response.text();
      })
      .then(data => {
            
        //document.getElementById( obj.dom ).innerHTML =  data;
        document.getElementById( "seccion_app" ).innerHTML =  data;          
        
        var api =  arasa.html.url.rel() + '/api/selectores/all' ;

        
        ajax.async.json( "get", api, null )
            .then(( xhr ) => {
                
                if (xhr.status == 200) {     

                    tabla.linea = "selector";    
                    tabla.campos = ['selector', 'descripcion', 'nivel', 'superior', 'codigo', 'ord'];        
                    tabla.tbody_id = "selectores-tb";           
                    
                    tabla.json = xhr.responseText;  
                    tabla.id =  "selectores-tabla"  ;   
                    //tabla.tbody_id = obj.tipo+"-tb";
                    tabla.gene();
                    
                    //tabla.lista_registro(  selector_registro  );
                    tabla.lista_registro(  obj, selector_registro  );
                    
                    
                    
                    
                    selector_lista_formato (obj, "seccion_app" );
                                        
                    
                    var btn_nuevo = document.getElementById( "btn_nuevo" );
                    btn_nuevo.addEventListener('click',
                        function(event) {      
                    
                            var url = arasa.html.url.rel() + obj.carpeta +'/'+ obj.tipo 
                                            + '/htmf/form.html'; 
                                    
                                                        
                            fetch( url )
                              .then(response => {
                                return response.text();
                              })
                              .then(data => {

                                //ajax.url = html.url.rel()+'/sistema/selector/htmf/form.html'; 
                                //ajax.url = '/app/mod/sistema/selector/htmf/form.html'; 
                                //ajax.metodo = "GET";       

                                modal.ancho = 600;
                                var ven = modal.ventana.mostrar("ind", data);   
                                selector_modal_add( obj, ven );
                                
                                
/*
                                var ven = modal.ventana.mostrar("ide", data);   
                                selector_modal_editdelete( obj, linea, ven  );    
*/

                              })    


                            
                            
                            
                                /*
                                
                            ajax.url = html.url.absolute() +'/sistema/selector/htmf/form.html'; 
                            ajax.metodo = "GET";  
                            modal.ancho = 600;
                            var obj = modal.ventana.mostrar("ind");       



                            
                            
                            */

                        },
                        false
                    );     

                    
                    
            
                }
                
            })    
            /*
            .catch(( xhr ) => {                                         
                console.log(xhr.message);                    
            }); 
              */      
                        
            
            //resolve( data );
      })    
    

}







function selector_modal_add ( obj, ven ){
    
    
    document.getElementById( "botones_borrar" ).style.display = 'none';
        
    
    var selector_superior = document.getElementById( "selector_superior" );
    selector_superior.value = 0;
    //selector_superior.disabled=true;    
    
    var selector_id = document.getElementById( "selector_selector" );
    selector_id.value = 0;
    //selector_id.disabled=true;
    
    var selector_ord = document.getElementById( "selector_ord" );
    selector_ord.value = 0;
        
        
        
        
        
    var btn_selmod_aceptar = document.getElementById( "btn_selmod_aceptar" );
    btn_selmod_aceptar.addEventListener('click',
        function(event) {     
                                    
            //    if ( main_form_validar() )
                if ( true )
                {
                                        
                    var url = arasa.html.url.rel()+"/api/selectores/";                                                 
                    form.name = "form_selector" ;                              
                    
                    loader.inicio();     
                    ajax.async.json( "post", url, form.datos.getjson() )
                        .then(( xhr ) => {

                            if (xhr.status == 200)
                            {                                  
                                btn_selmod_cancelar.click();
                                                        
                                selector_lista_all ( obj, ven );                                     
                                                                
                                loader.fin();      
                                msg.ok.mostrar("registro agregado");  
                                
                            }
                            else{
                                msg.error.mostrar("error");           
                            }       
                        }) 
                    
                }
                
        }
    );    
    
    
    
    
    
    
    
    var btn_selmod_cancelar = document.getElementById( "btn_selmod_cancelar" );
    btn_selmod_cancelar.addEventListener('click',
        function(event) {     
            modal.ventana.cerrar(ven);
        }
    );    
    
    
};







function selector_modal_editdelete ( obj, linea, ven ){
    
    
    var api =  arasa.html.url.rel() + '/api/selectores/'+linea;          

    ajax.async.json( "get", api, null )
        .then(( xhr ) => {

            if (xhr.status == 200)
            {     

                form.name = "form_selector" ;
                form.json = xhr.responseText;   

                form.campos = ['selector_id'];
                form.disabled(true);

                form.llenar();    
                

                // tabuladores
                var selectortab = new Selector();
                selectortab.tabuladores();




                var btn_selmod_aceptar = document.getElementById( "btn_selmod_aceptar" );
                btn_selmod_aceptar.innerHTML = "Editar";
                btn_selmod_aceptar.addEventListener('click',
                    function(event) {     

                        //if ( main_form_validar() )
                        if ( true )
                        {

                            var id = document.getElementById('selector_selector').value;                            
                            var url = arasa.html.url.rel()+"/api/selectores/"+id;                  
                            
                            
                            ajax.async.json( "put", url, form.datos.getjson() )
                                .then(( xhr ) => {

                                    if (xhr.status == 200)
                                    {                                          
                                        //tabla.json = xhr.responseText;     
                                        btn_selmod_cancelar.click();                                                                              
                                        selector_lista_all ( obj, obj.dom );                                              
                                        msg.ok.mostrar("registro editado");  
                                    }
                                    else{
                                        msg.error.mostrar("error de acceso");           
                                    }       
                                })    
                            
                            
                        }
                    }
                ); 



                var btn_selmod_borrar = document.getElementById( "btn_selmod_borrarr" );
                btn_selmod_borrar.addEventListener('click',
                    function(event) { 
                        

                        var id = document.getElementById('selector_selector').value;                        
                        var url = arasa.html.url.rel() + "/api/selectores/"+id;        
                        
                        ajax.async.json( "delete", url, null )
                            .then(( xhr ) => {


                                if (xhr.status == 200)
                                {  
                                    btn_selmod_cancelar.click();                                    
                                    selector_lista_all ( obj, obj.dom );      
                                    msg.ok.mostrar("registro borrado");       
                                    
                                }
                                else{
                                    msg.error.mostrar("error");                    
                                }       
                                
                            })    





/*
                        ajax.private.json( null );  

                        if (ajax.state == 200){

                            //  btn_selmod_cancelar.click();
                            selector_lista_all ( 'arti_form' );                        
                            msg.ok.mostrar("registro editado");                   
                        }
                        else{
                            msg.error.mostrar("error");           
                        }                 
*/




                    }
                );    




                var btn_selmod_cancelar = document.getElementById( "btn_selmod_cancelar" );
                btn_selmod_cancelar.addEventListener('click',
                    function(event) {     
                        modal.ventana.cerrar(ven);
                    }
                );    

                
                

            }

        })    

    
    






};





function selector_registro ( obj, linea ){

        
    var url = arasa.html.url.rel() + obj.carpeta +'/'+ obj.tipo 
            + '/htmf/form.html'; 
    
    fetch( url )
      .then(response => {
        return response.text();
      })
      .then(data => {
            
        modal.ancho = 600;        
        var ven = modal.ventana.mostrar("ide", data);           
        selector_modal_editdelete( obj, linea, ven  );    
        

        
        
        //selector_modal_editdelete( obj, linea  );    
        
/*        
*/
    
            //resolve( data );
            
            
      })    




    





};




function selector_lista_formato ( obj, table ){


    var table = document.getElementById( table ).getElementsByTagName('tbody')[0];
    var rows = table.getElementsByTagName('tr');

    for (var i=0 ; i < rows.length; i++)
    {
        cell = table.rows[i].cells[1] ;                                  
        //cell.innerHTML = fmtNum(cell.innerHTML);  
        cell.innerHTML = '&nbsp'.repeat(table.rows[i].cells[2].innerHTML * 5) + cell.innerHTML;            
    }



};







function main_lista ( dom  ){
};


function main_form_validar( ){
    return true;
};


function main_form_edit( ){    
};


function busquedalista( ){    
};




                





window.onresize = function() {
    document.body.style.minheight  = "100%" ;  
    var nodeList = document.querySelectorAll('.capaoscura');
    for (var i = 0; i < nodeList.length; ++i) {
        document.getElementById(nodeList[i].id).style.height = document.body.scrollHeight.toString() + "px";                
    }
};








