
var loader = 
{    
    dom: null,
    id:  "loader_id", 
   count: 0,
   
    inicio : function  ()
    {
        
        if ( loader.count == 0 ){
                
                

            var loade = document.createElement("div");
            loade.setAttribute("id", loader.id );
            loade.classList.add('loader');



            var loader_container = document.createElement("div");
            loader_container.classList.add('loader-container');

            var semicircle1 = document.createElement("div");
            semicircle1.classList.add('semicircle1');

            var semicircle2 = document.createElement("div");
            semicircle2.classList.add('semicircle2');

            loader_container.appendChild(semicircle1);
            loader_container.appendChild(semicircle2);

            loade.appendChild(loader_container);

            document.body.appendChild(loade);

            loader.dom = loade;
    
        
        }
        loader.count ++;

    },
   
    
    fin : function  ()
    {
        
        if (loader.count == 1) {
            document.body.removeChild( loader.dom );
        }
        loader.count --;
        
    }    
    
    
    
    
   
  
};



