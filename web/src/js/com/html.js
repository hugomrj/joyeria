
var html = 
{      
    sitio:  "",
    seccion_main : "seccion_app",
    appfolder: "/app/mod/",
    
    data: {  
        menu:"",
        user: 0,
        tokken: ""
        
    },
    
    
    menu: {               
        
        div: "menubar",

        ini: function( ) {

            var url = arasa.html.url.rel()+"/api/selectores/menu";           
            //ajax.metodo = "GET";     
                        
            ajax.async.json( "get", url, null )
                .then(( xhr ) => {

                    if (xhr.status == 200)
                    {     
                        
                        //html.data.user = ajax.private.json(null);      
                        html.data.user = xhr.responseText;                                                   
                        
                        html.data.menu = xhr.getResponseHeader("menu");
                                                
                        html.data.tokken = localStorage.getItem('token');
                        
                        html.menu.mostrar();

                    }
                    else{
                        // volver a login
                    }                  

                })            
                .catch(( xhr ) => {                                         
                    console.log(xhr.message);                    
                }); 

            
            
        }  ,                                

        mostrar: function(  ) {          
            
            document.getElementById( html.menu.div ).innerHTML 
                    = html.data.menu;
                    //=  localStorage.getItem('menu');                    
                        
            //redirect(ajax.state);            
            //alert(  window.location.pathname );            
            //  logo   header  <img src=\"../../ima/header/01.png\"/>
            
            document.getElementById( "header_logo" ).innerHTML  =
                    "<div></div>\n\
                    <div></div>\n\
                    <div></div>";            
            
        }  ,                                
    },
    
    url:{
        
        control: function(  fn ) {
            
            loader.inicio();
            
            var comp = window.location.pathname;            
            var carp = html.url.rel();
            var path =   comp.replace(carp, "");       
            
            xhr =  new XMLHttpRequest();            
            var url = html.url.rel()+"/api/usuarios/controlurl"; 
            var metodo = "GET";     

            xhr.open( metodo.toUpperCase(),   url,  true );      
        
            xhr.onreadystatechange = function () {
                if (this.readyState == 4 ){

                    //ajax.headers.get();
                    ajax.local.token =  xhr.getResponseHeader("token") ;            
                    localStorage.setItem('token', xhr.getResponseHeader("token") );     
      
                    ajax.state = xhr.status;
                    
                    html.url.redirect(ajax.state);   
                    
                    fn();
                    
                    loader.fin();
                    
                    
                    // cabecera basica
                    //html.barra_superior();


                }
            };
            xhr.onerror = function (e) {  
              alert("error");
            };         
      
            xhr.setRequestHeader("path", path );
            var type = "application/json";
            xhr.setRequestHeader('Content-Type', type);   
            //ajax.headers.set();
            xhr.setRequestHeader("token", localStorage.getItem('token'));           
      
            xhr.send( null );                                            
            
        } ,     
        
               
        
        
  
                
                
                
        redirect: function(val) 
        {
            
            switch(val) {
                case 200:
                    break;                    
                case 401:
                    window.location = html.url.rel();                                     
                case 500:
                    window.location = html.url.rel();                 
                    break;            
                default:
                    window.location = html.url.rel();                 
            }

        },                  
                
        
    },
    
    
    barra_superior: function(  ) {  


        ajax.url = html.url.rel() + '/basicas/barrasuperior/barrasuperior.html';   
        ajax.metodo = "GET";            
        document.getElementById( "barrasuperior" ).innerHTML =  ajax.public.html();        
        
        document.getElementById( "session_usuario_cab" ).innerHTML = 
                xhr.getResponseHeader("sessionusuario");

        
        document.getElementById( "session_sucursal_cab" ).innerHTML = 
                xhr.getResponseHeader("sessionsucursal");    
    
    
    },





};





